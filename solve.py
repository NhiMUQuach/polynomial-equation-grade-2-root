from math import sqrt
import sys

def solve(args):
  """
  return the real solution of the following equation
  ax^2 + bx +  c = 0  
  """
  a, b, c = args
  a = int(a)
  b = int(b)
  c = int(c)
  D = b*b - 4*a*c
  if D < 0:
    return None
  if D == 0:
    return (-b/2/a,-b/2/a)
  if D > 0:
    return (-b/2/a - sqrt(D)/2/a,  -b/2/a + sqrt(D)/2/a)


if __name__ == '__main__':
  print(solve(sys.argv[1:]))
